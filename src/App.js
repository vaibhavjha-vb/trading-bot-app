import "./App.css";

import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from "react-router-dom";
import List from "./components/List";
import Details from "./components/Desc";
import { useSelector } from "react-redux";
function App() {
  const res = useSelector((state) => state);
  console.log(res);
  return (
    <div className="App">
    <Router>
      <Switch>
        <Redirect exact from="/" to="/bots" />
        <Route exact path="/bots" component={List} />
        <Route exact path="/bots-details/:botId" component={Details} />
      </Switch>
    </Router>
    </div>
  );
}

export default App;
