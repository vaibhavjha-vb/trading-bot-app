import { createStore } from "redux";
import botReducer from "./Reducer";

const store = createStore(botReducer);

export default store;
