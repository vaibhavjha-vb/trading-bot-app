import React from "react";
import { useSelector } from "react-redux";
import Head from "./Header";
import Bot from "./Bots";
function BotList() {
  const botData = useSelector((state) => state.botData);
  return (
    <div>
      <Head />
      <h2>Bots List</h2>
      {botData.map((item) => {
        return (
          <Bot
            id={item.id}
            title={item.bot}
            desc={item.description}
            cagr={item.cagr}
          />
        );
      })}
    </div>
  );
}
export default BotList;
