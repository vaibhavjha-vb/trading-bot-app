import React from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

function Bots(props) {
  const dispatch = useDispatch();
  const history = useHistory();
  const addToCart = () => {
    dispatch({
      type: "addToCart",
      payload: {
        id: props.id,
        bot: props.title,
        description: props.desc,
        index: props.index,
        cagr: props.cagr,
      },
    });
  };
  const showAlgo = () => {
    history.push("/bots-details/" + props.id);
  };

  return (
    <div style={{ backgroundColor: "orchid" }}>
      <h2>Bot Name: {props.title}</h2>
      <h2>Description: {props.desc}</h2>
      {/* <h2>Index Value: {props.index}</h2> */}
      <h2>CAGR: {props.cagr}</h2>
      <button onClick={showAlgo}>Show Algo </button>
      <button onClick={addToCart}>Add To Cart</button>
    </div>
  );
}
export default Bots;
