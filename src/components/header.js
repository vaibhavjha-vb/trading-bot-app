import React from "react";
import { useSelector } from "react-redux";

function Dashboard() {
  const cartItems = useSelector((state) => state.cart.length);
  console.log(cartItems);
  return (
    <div className="dash-container">
      <p>Dashboard</p>
      <p>Cart {cartItems}</p>
    </div>
  );
}

export default Dashboard;
