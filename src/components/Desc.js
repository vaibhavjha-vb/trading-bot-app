import AppBar from "./Header";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";

function BotDetails() {
  const { botId } = useParams();
  let bot = "";
  let botDesc = "";
  let botCagr = 0;
  const algoDesc = useSelector((state) => {
    state.botData.forEach((element) => {
      if (element.id === botId) {
        bot = element.bot;
        botDesc = element.description;
        botCagr = element.cagr;
      }
    });
  });
  return (
    <div>
      <AppBar />
      <h2>Bot details</h2>
      <h2>id{botId}</h2>
      <h2>{bot}</h2>
      <h2>{botDesc}</h2>
      <h2>{botCagr}</h2>
      {/* <button onClick={addToCart}>Add To Cart</button> */}
    </div>
  );
}

export default BotDetails;
